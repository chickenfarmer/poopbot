#include "Bot.h"

Bot::Bot()
{
	this->hWndWow = NULL;
	this->hWow    = INVALID_HANDLE_VALUE;

	// Initialize Static Addresses
	this->baseAddr                    = 0x400000;
	this->staticClientConnectionAddr  = this->baseAddr + 0x00741414;
	this->clientConnectionAddr        = 0;
	this->firstObjectAddr             = 0;

	for (unsigned int i = 0; i < 100; i++)
	{
		this->players[i]     = NULL;
		this->npcs[i]        = NULL;
		this->bannedGuids[i] = 0;
	}

	this->targetGuid = 0;

	this->playerHasTarget   = FALSE;
	this->playerIsMoving    = FALSE;
	this->playerIsTurning   = FALSE;
	this->playerIsAttacking = FALSE;
}

void Bot::Initialize()
{
	this->hWndWow = FindWindow(NULL, "World of Warcraft");

	if (this->hWndWow == NULL)
	{
		std::cout << "Unable to find WoW window.\n";
		return;
	}

	std::cout << "WoW window found!\n";

	DWORD pidWow;

	GetWindowThreadProcessId(this->hWndWow, &pidWow);

	std::cout << "WoW Process ID: " << pidWow << "\n";

	// Enable Debug Privileges
	HANDLE hToken;

	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
	{
		std::cout << "Failed to open process token!\n";
		return;
	}

	TOKEN_PRIVILEGES tp = {0};
	LUID luid;

	if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid))
	{
		std::cout << "Failed to look up privilege value!\n";
		return;
	}

	tp.PrivilegeCount           = 1;
	tp.Privileges[0].Luid       = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if (!AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL))
	{
		std::cout << "Failed to adjust token privileges!\n";
		return;
	}

	this->hWow = OpenProcess(PROCESS_VM_READ | PROCESS_QUERY_INFORMATION, FALSE, pidWow);

	if (this->hWow == NULL)
	{
		std::cout << "Unable to open WoW process. Error: " << GetLastError() << "\n";
		return;
	}

	this->wi.cbSize = sizeof(WINDOWINFO);

	GetWindowInfo(this->hWndWow, &(this->wi));

	BOOL iResult = FALSE;

	iResult = ReadProcessMemory(this->hWow, (void *) this->staticClientConnectionAddr, &(this->clientConnectionAddr), sizeof(this->clientConnectionAddr), NULL);

	if (iResult == FALSE)
	{
		std::cout << "Failed to read client connection address.\n";
		return;
	}

	iResult = ReadProcessMemory(this->hWow, (void *) (this->clientConnectionAddr + 0xAC), &(this->firstObjectAddr), sizeof(this->firstObjectAddr), NULL);

	if (iResult == FALSE)
	{
		std::cout << "Failed to read first object address.\n";
		return;
	}
}

void Bot::LoadObjects()
{
	unsigned int currentObjectAddr = this->firstObjectAddr;
	unsigned int objectCount       = 0;
	unsigned int playerCount       = 0;
	unsigned int npcCount          = 0;

	for (unsigned int i = 0; i < 100; i++)
	{
		if (this->players[i] != NULL)
		{
			delete this->players[i];
		}

		if (this->npcs[i] != NULL)
		{
			delete this->npcs[i];
		}

		this->players[i] = NULL;
		this->npcs[i]    = NULL;
	}

	while (currentObjectAddr != 0 && currentObjectAddr % 2 == 0)
	{
		// Determine Object's Type
		unsigned int typeAddr = currentObjectAddr + 0x14;
		short type            = 0;
		BOOL iResult          = FALSE;

		iResult = ReadProcessMemory(this->hWow, (void *) typeAddr, &type, sizeof(type), NULL);

		if (iResult == FALSE)
		{
			std::cout << "Failed to read type!\n";
		}

		else
		{
			if (type == WOT_PLAYER)
			{
				this->players[playerCount] = new WObject::Player(this->hWow, currentObjectAddr);
				this->players[playerCount]->RetrieveData();
				playerCount = playerCount + 1;
			}

			else if (type == WOT_UNIT)
			{
				this->npcs[npcCount] = new WObject::NPC(this->hWow, currentObjectAddr);
				this->npcs[npcCount]->RetrieveData();
				npcCount = npcCount + 1;
			}
		}

		objectCount = objectCount + 1;

		unsigned int nextObjectAddr = currentObjectAddr + 0x3C;

		iResult = ReadProcessMemory(this->hWow, (void *) nextObjectAddr, &currentObjectAddr, sizeof(currentObjectAddr), NULL);

		if (iResult == FALSE)
		{
			break;
		}
	}
}

void Bot::Grind()
{
	this->playerHasTarget   = FALSE;
	this->playerIsMoving    = FALSE;
	this->playerIsTurning   = FALSE;
	this->playerIsAttacking = FALSE;

	this->LoadObjects();

	if (this->players[0]->IsInCombat())
	{
		std::cout << "In combat!\n";
		this->targetGuid      = this->FindNearestAggressiveNpc();
		WObject::NPC * target = this->FindNpcByGuid(this->targetGuid);

		if (target == NULL)
		{
			return;
		}

		std::cout << "Closest aggro'd NPC is " << target->GUID << "\n";

		while (this->players[0]->target != target->GUID)
		{
			this->TargetNpc();

			this->LoadObjects();
			target = this->FindNpcByGuid(this->targetGuid);

			Sleep(1150);
		}

		
		while (target->health > 0 && target != NULL)
		{
			this->TravelToObject(target);
			this->AttackNpc(target);

			this->LoadObjects();
			target = this->FindNpcByGuid(this->targetGuid);

			Sleep(1150);
		}
		
	}

	else
	{
		if (this->players[0]->health > (0.75 * this->players[0]->maxHealth))
		{
			this->targetGuid = this->FindNearestNpc();

			while (this->FindNpcByGuid(this->targetGuid)->health > 0)
			{
				this->LoadObjects();

				WObject::NPC * target = this->FindNpcByGuid(this->targetGuid);

				if (target == NULL)
				{
					break;
				}

				float angleDiff = abs(this->players[0]->rotation - this->CalculateAngle(this->players[0], target));

				if (angleDiff > 0.15)
				{
					this->RotatePlayer();
					this->MovePlayer(TRUE);
				}

				else
				{
					this->RotatePlayer(TRUE);

					float distance = this->CalculateDistance(this->players[0], target);

					if (distance > 3.5)
					{
						this->MovePlayer();
					}

					else
					{
						this->MovePlayer(TRUE);

						if (this->players[0]->target != target->GUID)
						{
							
							SendMessage(this->hWndWow, WM_KEYDOWN, VK_TAB, NULL);
							SendMessage(this->hWndWow, WM_KEYUP, VK_TAB, NULL);

							Sleep(1150);

							this->LoadObjects();
							target = this->FindNpcByGuid(this->targetGuid);

							if (target == NULL)
							{
								break;
							}

							if (this->players[0]->target != target->GUID)
							{
								std::cout << "Unable to acquire target of " << target->GUID << "\n";
								this->BanGuid(target->GUID);
								break;
							}

							SendMessage(this->hWndWow, WM_KEYDOWN, 0x31, NULL);
							SendMessage(this->hWndWow, WM_KEYUP, 0x31, NULL);
							this->playerIsAttacking = TRUE;
						}

						else
						{
							this->AttackNpc(target);
						}
					}
				}
			}

			Sleep(1000);
		}

		else
		{
		}
	}

		DWORD clickCoords = 0;

		//clickCoords = MAKEWORD(605, 332);
		//SendMessage(this->hWndWow, WM_KEYDOWN, VK_SHIFT, NULL);
		//SendMessage(this->hWndWow, WM_RBUTTONDOWN, MK_RBUTTON, clickCoords);
		//SendMessage(this->hWndWow, WM_RBUTTONUP, MK_RBUTTON, clickCoords);
		//SendMessage(this->hWndWow, WM_KEYUP, VK_SHIFT, NULL);

		//std::cout << "R-Click at (" << this->wi.rcClient.right / 2 << ", " << (this->wi.rcClient.bottom / 2) + 10 << ")\n";

	//POINT p;

	//GetCursorPos(&p);
	//ScreenToClient(this->hWndWow, &p);

	//std::cout << "(" << p.x << ", " << p.y << ")\n";
}

float Bot::CalculateDistance(WowObject * o1, WowObject * o2)
{
	if ((o1->type == WOT_PLAYER || o1->type == WOT_UNIT) && (o2->type == WOT_PLAYER || o2->type == WOT_UNIT))
	{
		float x = o1->x - o2->x;
		float y = o1->y - o2->y;
		float z = o1->z - o2->z;

		return sqrt((x * x) + (y * y) + (z * z));
	}

	return 0;
}

float Bot::CalculateAngle(WowObject * o1, WowObject * o2)
{
	float x   = o2->x - o1->x;
	float y   = o2->y - o1->y;
	float t   = abs(atan(x / y));
	float r   = 0;

	if (o1->x > o2->x)
	{
		if (o1->y > o2->y)
		{
			r = M_PI + t;
		}

		else
		{
			r = (2 * M_PI) - t;
		}
	}

	else
	{
		if (o1->y > o2->y)
		{
			r = M_PI - t;
		}

		else
		{
			r = t;
		}
	}

	return r;
}

BOOL Bot::PlayerAtObject(WowObject * obj)
{
	if (this->CalculateDistance(this->players[0], obj) > 4.0)
	{
		return FALSE;
	}

	return TRUE;
}

void Bot::TravelToObject(WowObject * obj)
{
	this->playerHasTarget = FALSE;
	float desiredAngle    = this->CalculateAngle(this->players[0], obj);
	float angleDiff       = abs(desiredAngle - this->players[0]->rotation);
	float tolerance       = 0.15f;
	float distance        = this->CalculateDistance(this->players[0], obj);

	while (distance > 3.5 || angleDiff > tolerance)
	{
		this->LoadObjects();
		obj = this->FindNpcByGuid(this->targetGuid);

		angleDiff = abs(this->CalculateAngle(this->players[0], obj) - this->players[0]->rotation);
		distance  = this->CalculateDistance(this->players[0], obj);

		if (angleDiff > tolerance)
		{
			this->RotatePlayer();
		}

		else
		{
			this->RotatePlayer(TRUE);

			if (distance > 3.5)
			{
				this->MovePlayer();
			}

			else
			{
				this->MovePlayer(TRUE);
			}
		}
	}

	this->RotatePlayer(TRUE);
	this->MovePlayer(TRUE);
}

void Bot::FaceNpc(WowObject * obj)
{
	float desiredRotation = 0;
	float tolerance       = 12.5f * M_PI / 180.0f;

	if (obj->rotation > M_PI)
	{
		desiredRotation = obj->rotation - M_PI;
	}

	else
	{
		desiredRotation = obj->rotation + M_PI;
	}

	if (abs(desiredRotation - this->players[0]->rotation) > tolerance)
	{
		this->RotatePlayer();
	}

	else
	{
		this->RotatePlayer(TRUE);
	}
}

void Bot::TargetNpc()
{
	std::cout << "Attempting to target NPC...\n";
	PostMessage(this->hWndWow, WM_KEYDOWN, VK_TAB, NULL);
	SendMessage(this->hWndWow, WM_KEYUP, VK_TAB, NULL);
}

void Bot::AttackNpc(WObject::NPC * npc)
{
	if (this->playerIsAttacking)
	{
		SendMessage(this->hWndWow, WM_KEYDOWN, 0x32, NULL);
		SendMessage(this->hWndWow, WM_KEYUP, 0x32, NULL);
	}

	else
	{
		SendMessage(this->hWndWow, WM_KEYDOWN, 0x31, NULL);
		SendMessage(this->hWndWow, WM_KEYUP, 0x31, NULL);
		this->playerIsAttacking = TRUE;
	}
}

void Bot::BanGuid(unsigned long long guid)
{
	for (unsigned int i = 0; i < 100; i++)
	{
		if (this->bannedGuids[i] == 0)
		{
			this->bannedGuids[i] = guid;
			break;
		}
	}
}

BOOL Bot::FindBannedGuid(unsigned long long guid)
{
	for (unsigned int i = 0; i < 100; i++)
	{
		if (this->bannedGuids[i] == guid)
		{
			return TRUE;
		}
	}

	return FALSE;
}

WObject::NPC * Bot::FindNpcByGuid(unsigned long long GUID)
{
	for (unsigned int i = 0; this->npcs[i] != NULL; i++)
	{
		if (this->npcs[i]->GUID == GUID && this->npcs[i]->type == WOT_UNIT)
		{
			return this->npcs[i];
		}
	}

	return NULL;
}

unsigned long long Bot::FindNearestNpc()
{
	float distance    = 5000;
	float newDistance = 5000;
	unsigned int id   = 0;

	for (unsigned int i = 0; this->npcs[i] != NULL; i++)
	{
		if (this->npcs[i]->type == WOT_UNIT && !(this->FindBannedGuid(this->npcs[i]->GUID)))
		{
			newDistance = this->CalculateDistance(this->players[0], this->npcs[i]);

			if ((newDistance > 0) && (newDistance < distance))
			{
				distance = newDistance;
				id       = i;
			}
		}
	}

	return this->npcs[id]->GUID;
}

unsigned long long Bot::FindNearestAggressiveNpc()
{
	unsigned int distance    = 1000;
	unsigned int newDistance = 0;
	unsigned int id          = 1000;

	for (unsigned int i = 0; i < 100; i++)
	{
		if (this->npcs[i] != NULL && this->npcs[i]->target == this->players[0]->GUID && this->npcs[i]->IsInCombat())
		{
			newDistance = this->CalculateDistance(this->players[0], this->npcs[i]);

			if (newDistance < distance)
			{
				distance = newDistance;
				id       = i;
			}
		}
	}

	if (id == 1000)
	{
		return 0;
	}

	return this->npcs[id]->GUID;
}

void Bot::MovePlayer(BOOL stop)
{
	if (stop)
	{
		if (this->playerIsMoving)
		{
			std::cout << "Player stopped moving.\n";
			this->playerIsMoving = FALSE;
			SendMessage(this->hWndWow, WM_KEYUP, VK_UP, NULL);
		}
	}

	else
	{
		if (this->playerIsMoving == FALSE)
		{
			std::cout << "Player started moving.\n";
			this->playerIsMoving = TRUE;
			SendMessage(this->hWndWow, WM_KEYDOWN, VK_UP, NULL);
		}
	}
}

void Bot::RotatePlayer(BOOL stop)
{
	if (stop)
	{
		if (this->playerIsTurning)
		{
			std::cout << "Player stopped turning.\n";
			this->playerIsTurning = FALSE;
			SendMessage(this->hWndWow, WM_KEYUP, VK_RIGHT, NULL);
		}
	}

	else
	{
		if (this->playerIsTurning == FALSE)
		{
			std::cout << "Player started turning.\n";
			this->playerIsTurning = TRUE;
			SendMessage(this->hWndWow, WM_KEYDOWN, VK_RIGHT, NULL);
		}
	}
}