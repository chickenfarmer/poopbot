#ifndef BOT_H
#define BOT_H

#include "Main.h"
#include "WowObject.h"
#include "WowObject/Player.h"
#include "WowObject/NPC.h"

class Bot
{
	public:
		Bot();
		~Bot();

		void Initialize();
		void LoadObjects();
		void Grind();
		void Farm();
		BOOL PlayerAtObject(WowObject * obj);
		void TravelToObject(WowObject * obj);
		void FaceNpc(WowObject * obj);
		void TargetNpc();
		void AttackNpc(WObject::NPC * npc);
		void SendChatMessage(std::string msg);

		unsigned long long FindNearestNpc();
		unsigned long long FindNearestAggressiveNpc();
		WObject::NPC * FindNpcByGuid(unsigned long long GUID);

		void BanGuid(unsigned long long guid);
		BOOL FindBannedGuid(unsigned long long guid);

		unsigned long long targetGuid;

		void MovePlayer(BOOL stop = FALSE);
		void RotatePlayer(BOOL stop = FALSE);

		// Members
		WObject::Player * players[100];
		WObject::NPC * npcs[100];

		HWND hWndWow;
		HANDLE hWow;

		WINDOWINFO wi;

		unsigned int baseAddr;
		unsigned int staticClientConnectionAddr;
		unsigned int clientConnectionAddr;
		unsigned int firstObjectAddr;

	private:
		float CalculateDistance(WowObject * o1, WowObject * o2);
		float CalculateAngle(WowObject * o1, WowObject * o2);

		BOOL playerIsMoving;
		BOOL playerIsTurning;
		BOOL playerHasTarget;
		BOOL playerIsAttacking;

		unsigned long long bannedGuids[100];
};

#endif