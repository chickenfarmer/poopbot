#include "Main.h"

#include "Bot.h"

unsigned int baseAddr = 0x400000;
unsigned int sccAddr  = baseAddr + 0x00741414;

Bot * bot;

void SendChatMessage(const char * str, const unsigned int len)
{
	//SendMessage(hSam, WM_KEYDOWN, VK_RETURN, NULL);
	//SendMessage(hSam, WM_KEYUP, VK_RETURN, NULL);

	for (unsigned int i = 0; i < len; i++)
	{
		SendMessage(bot->hWndWow, WM_CHAR, str[i], NULL);
	}

	SendMessage(bot->hWndWow, WM_KEYDOWN, VK_RETURN, NULL);
	SendMessage(bot->hWndWow, WM_KEYUP, VK_RETURN, NULL);

}

int main(int argc, char * argv[])
{
	bot = new Bot();
	bot->Initialize();
	bot->LoadObjects();

	DWORD time1 = GetTickCount();

	while (1)
	{
		bot->LoadObjects();
		bot->Grind();
	}

	DWORD time2 = GetTickCount();

	std::cout << "Bot ran for " << time2 - time1 << " milliseconds!\n";

	return (0);
}