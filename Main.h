#ifndef MAIN_H
#define MAIN_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <Windows.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <sstream>
#include <math.h>

#endif