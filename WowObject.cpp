#include "WowObject.h"

WowObject::WowObject()
{
	
}

BOOL WowObject::IsInCombat()
{
	return (this->flags & 0x80000);
}

void WowObject::RetrieveUnitFieldsAddress()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->baseAddress + 0x8), &(this->unitFieldsAddress), sizeof(this->unitFieldsAddress), NULL);

	if (iResult == FALSE)
	{
		this->unitFieldsAddress = 0;
		std::cout << "Error retrieving unit fields address!\n";
	}
}

void WowObject::RetrieveGUID()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->baseAddress + 0x30), &(this->GUID), sizeof(this->GUID), NULL);

	if (iResult == FALSE)
	{
		this->GUID = 0;
		std::cout << "Error retrieving GUID!\n";
	}
}

void WowObject::RetrieveName()
{
}

void WowObject::RetrieveLevel()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x88), &(this->level), sizeof(this->level), NULL);

	if (iResult == FALSE)
	{
		this->level = 0;
		std::cout << "Error retrieving level!\n";
	}
}

void WowObject::RetrieveHealth()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x58), &(this->health), sizeof(this->health), NULL);

	if (iResult == FALSE)
	{
		this->health = 0;
		std::cout << "Error retrieving health!\n";
	}
}

void WowObject::RetrieveMaxHealth()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x70), &(this->maxHealth), sizeof(this->maxHealth), NULL);

	if (iResult == FALSE)
	{
		this->maxHealth = 0;
		std::cout << "Error retrieving max health!\n";
	}
}

void WowObject::RetrievePower1()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x5C), &(this->power1), sizeof(this->power1), NULL);

	if (iResult == FALSE)
	{
		this->power1 = 0;
		std::cout << "Error retrieving power1!\n";
	}
}

void WowObject::RetrievePower2()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x60), &(this->power2), sizeof(this->power2), NULL);

	if (iResult == FALSE)
	{
		this->power2 = 0;
		std::cout << "Error retrieve power2!\n";
	}
}

void WowObject::RetrievePower3()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x64), &(this->power3), sizeof(this->power3), NULL);

	if (iResult == FALSE)
	{
		this->power3 = 0;
		std::cout << "Error retrieve power3!\n";
	}
}

void WowObject::RetrievePower4()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x68), &(this->power4), sizeof(this->power4), NULL);

	if (iResult == FALSE)
	{
		this->power4 = 0;
		std::cout << "Error retrieve power4!\n";
	}
}

void WowObject::RetrievePower5()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x6C), &(this->power5), sizeof(this->power5), NULL);

	if (iResult == FALSE)
	{
		this->power5 = 0;
		std::cout << "Error retrieve power5!\n";
	}
}

void WowObject::RetrieveMaxPower1()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x74), &(this->maxPower1), sizeof(this->maxPower1), NULL);

	if (iResult == FALSE)
	{
		this->maxPower1 = 0;
		std::cout << "Error retrieving max power!\n";
	}
}

void WowObject::RetrieveMaxPower2()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x78), &(this->maxPower2), sizeof(this->maxPower2), NULL);

	if (iResult == FALSE)
	{
		this->maxPower2 = 0;
		std::cout << "Error retrieving max power!\n";
	}
}

void WowObject::RetrieveMaxPower3()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x7C), &(this->maxPower3), sizeof(this->maxPower3), NULL);

	if (iResult == FALSE)
	{
		this->maxPower3 = 0;
		std::cout << "Error retrieving max power!\n";
	}
}

void WowObject::RetrieveMaxPower4()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x80), &(this->maxPower4), sizeof(this->maxPower4), NULL);

	if (iResult == FALSE)
	{
		this->maxPower4 = 0;
		std::cout << "Error retrieving max power!\n";
	}
}

void WowObject::RetrieveMaxPower5()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x84), &(this->maxPower5), sizeof(this->maxPower5), NULL);

	if (iResult == FALSE)
	{
		this->maxPower5 = 0;
		std::cout << "Error retrieving max power!\n";
	}
}

void WowObject::RetrieveFlags()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0xB8), &(this->flags), sizeof(this->flags), NULL);

	if (iResult == FALSE)
	{
		this->flags = 0;
		std::cout << "Error retrieving flags!\n";
	}
}

void WowObject::RetrieveNpcFlags()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x24C), &(this->npcFlags), sizeof(this->npcFlags), NULL);

	if (iResult == FALSE)
	{
		this->npcFlags = 0;
		std::cout << "Error retrieving NPC flags!\n";
	}
}

void WowObject::RetrieveFactionTemplate()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x8C), &(this->factionTemplate), sizeof(this->factionTemplate), NULL);

	if (iResult == FALSE)
	{
		this->factionTemplate = 0;
		std::cout << "Error retrieving faction template!\n";
	}
}

void WowObject::RetrieveTarget()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->unitFieldsAddress + 0x40), &(this->target), sizeof(this->target), NULL);

	if (iResult == FALSE)
	{
		this->target = 0;
		std::cout << "Error retrieving target!\n";
	}
}

void WowObject::RetrieveX()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->baseAddress + 0x9B8 + 0x4), &(this->x), sizeof(this->x), NULL);

	if (iResult == FALSE)
	{
		this->x = 0;
		std::cout << "Error retrieving X!\n";
	}
}

void WowObject::RetrieveY()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->baseAddress + 0x9B8), &(this->y), sizeof(this->y), NULL);

	if (iResult == FALSE)
	{
		this->y = 0;
		std::cout << "Error retrieving Y!\n";
	}
}

void WowObject::RetrieveZ()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->baseAddress + 0x9B8 + 0x8), &(this->z), sizeof(this->z), NULL);

	if (iResult == FALSE)
	{
		this->z = 0;
		std::cout << "Error retrieving Z!\n";
	}
}

void WowObject::RetrieveRotation()
{
	BOOL iResult = ReadProcessMemory(this->hWow, (void *) (this->baseAddress + 0x9C4), &(this->rotation), sizeof(this->rotation), NULL);

	if (iResult == FALSE)
	{
		this->rotation = 0;
		std::cout << "Error retrieving rotation!\n";
	}
}