#ifndef WOW_OBJECT_H
#define WOW_OBJECT_H

#include "Main.h"

enum WowObjectType 
{
	WOT_NONE      = 0, 
	WOT_ITEM      = 1,
	WOT_CONTAINER = 2,
	WOT_UNIT      = 3,
	WOT_PLAYER    = 4,
	WOT_GAMEOBJ   = 5,
	WOT_DYNOBJ    = 6,
	WOT_CORPSE    = 7
};

class WowObject
{
	public:
		WowObject();

		// Methods
		void RetrieveUnitFieldsAddress();
		void RetrieveGUID();
		void RetrieveType();
		void RetrieveName();

		BOOL IsInCombat();

		// Members
		HANDLE hWow;
		unsigned int baseAddress;
		unsigned int unitFieldsAddress;
		unsigned long long GUID;
		short type;
		std::string name;

		unsigned int level;
		unsigned int health;
		unsigned int maxHealth;
		unsigned int power1;
		unsigned int power2;
		unsigned int power3;
		unsigned int power4;
		unsigned int power5;
		unsigned int maxPower1;
		unsigned int maxPower2;
		unsigned int maxPower3;
		unsigned int maxPower4;
		unsigned int maxPower5;

		unsigned int flags;
		unsigned int npcFlags;
		unsigned int factionTemplate;

		unsigned long long target;


		float x;
		float y;
		float z;
		float rotation;

	protected:

		void RetrieveLevel();
		void RetrieveHealth();
		void RetrieveMaxHealth();
		void RetrievePower1();
		void RetrievePower2();
		void RetrievePower3();
		void RetrievePower4();
		void RetrievePower5();
		void RetrieveMaxPower1();
		void RetrieveMaxPower2();
		void RetrieveMaxPower3();
		void RetrieveMaxPower4();
		void RetrieveMaxPower5();
		void RetrieveFlags();
		void RetrieveNpcFlags();
		void RetrieveFactionTemplate();
		void RetrieveTarget();

		void RetrieveX();
		void RetrieveY();
		void RetrieveZ();
		void RetrieveRotation();

	private:
};

#endif