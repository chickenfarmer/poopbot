#include "NPC.h"

WObject::NPC::NPC(HANDLE hWow, unsigned int baseAddress)
{
	this->hWow        = hWow;
	this->baseAddress = baseAddress;
	this->type        = WOT_UNIT;

	this->RetrieveUnitFieldsAddress();
	this->RetrieveGUID();
}

void WObject::NPC::RetrieveData()
{
	this->RetrieveLevel();
	this->RetrieveHealth();
	this->RetrieveMaxHealth();
	this->RetrievePower1();
	this->RetrievePower2();
	this->RetrievePower3();
	this->RetrievePower4();
	this->RetrievePower5();
	this->RetrieveMaxPower1();
	this->RetrieveMaxPower2();
	this->RetrieveMaxPower3();
	this->RetrieveMaxPower4();
	this->RetrieveMaxPower5();
	this->RetrieveFlags();
	this->RetrieveNpcFlags();
	this->RetrieveTarget();
	this->RetrieveFactionTemplate();

	this->RetrieveX();
	this->RetrieveY();
	this->RetrieveZ();
	this->RetrieveRotation();
}