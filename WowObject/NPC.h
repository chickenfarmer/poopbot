#ifndef WOW_OBJECT_NPC_H
#define WOW_OBJECT_NPC_H

#include "../Main.h"
#include "../WowObject.h"

namespace WObject
{
	class NPC : public WowObject
	{
		public:
			NPC(HANDLE hWow, unsigned int baseAddress);

			// Methods
			void RetrieveData();

		private:
	};
}

#endif