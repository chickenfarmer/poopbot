#include "Player.h"

WObject::Player::Player(HANDLE hWow, unsigned int baseAddress)
{
	this->hWow        = hWow;
	this->baseAddress = baseAddress;
	this->type        = WOT_PLAYER;

	this->RetrieveUnitFieldsAddress();
	this->RetrieveGUID();
}

void WObject::Player::RetrieveData()
{
	if (this->baseAddress != 0 && this->unitFieldsAddress != 0)
	{
		this->RetrieveLevel();
		this->RetrieveHealth();
		this->RetrieveMaxHealth();
		this->RetrievePower1();
		this->RetrievePower2();
		this->RetrievePower3();
		this->RetrievePower4();
		this->RetrievePower5();
		this->RetrieveMaxPower1();
		this->RetrieveMaxPower2();
		this->RetrieveMaxPower3();
		this->RetrieveMaxPower4();
		this->RetrieveMaxPower5();
		this->RetrieveFlags();
		this->RetrieveTarget();
		this->RetrieveFactionTemplate();

		this->RetrieveX();
		this->RetrieveY();
		this->RetrieveZ();
		this->RetrieveRotation();
	}
}