#ifndef WOW_OBJECT_PLAYER_H
#define WOW_OBJECT_PLAYER_H

#include "../Main.h"
#include "../WowObject.h"

namespace WObject
{
	class Player : public WowObject
	{
		public:
			Player(HANDLE hWow, unsigned int baseAddress);

			// Methods
			void RetrieveData();

			BOOL IsLooting();
			

		private:

			// Methods
			
	};
}

#endif